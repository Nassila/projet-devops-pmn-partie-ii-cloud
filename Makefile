.PHONY: help
.DEFAULT_GOAL = help

ROOT_DIR:=$(shell pwd)
POD_ID=

## —— Docker compose dolibarr 🐳 ———————————————————————————————————————————————————————————————————
run: ## run docker-compose with dolibarr and mariadb services 
	docker-compose -f docker/docker-compose.yml up

## —— Init 📥 ———————————————————————————————————————————————————————————————————
init: ## Init only credentials
	docker run -v aws:/root/.aws -v $(ROOT_DIR):/apps nassila94h/k8s_aws:latest aws configure import --csv file://credentials-local.csv

## —— Deployment 🖥 ———————————————————————————————————————————————————————————————————
deploy: init ## Init cluster and deploy
	docker run -v kube:/root/.kube -v aws:/root/.aws -v $(ROOT_DIR):/apps nassila94h/k8s_aws:latest eksctl create cluster --name projet-group2-dolibarr --region eu-west-3 --nodes=3 --nodes-min=1 --nodes-max=4 --node-type=t2.micro
	docker run -v kube:/root/.kube -v aws:/root/.aws -v $(ROOT_DIR):/apps nassila94h/k8s_aws:latest kubectl apply -f kubernetes/dolibarr-service.yaml
	docker run -v kube:/root/.kube -v aws:/root/.aws -v $(ROOT_DIR):/apps nassila94h/k8s_aws:latest kubectl apply -f kubernetes/mariadb-service.yaml
	docker run -v kube:/root/.kube -v aws:/root/.aws -v $(ROOT_DIR):/apps nassila94h/k8s_aws:latest kubectl apply -f kubernetes/load-balancer-service.yaml
	docker run -v kube:/root/.kube -v aws:/root/.aws -v $(ROOT_DIR):/apps nassila94h/k8s_aws:latest kubectl apply -f kubernetes/dolibarr-deployment.yaml
	docker run -v kube:/root/.kube -v aws:/root/.aws -v $(ROOT_DIR):/apps nassila94h/k8s_aws:latest kubectl apply -f kubernetes/mariadb-deployment.yaml
	docker run -v kube:/root/.kube -v aws:/root/.aws -v $(ROOT_DIR):/apps nassila94h/k8s_aws:latest kubectl apply -f kubernetes/net-networkpolicy.yaml
	docker run -v kube:/root/.kube -v aws:/root/.aws -v $(ROOT_DIR):/apps nassila94h/k8s_aws:latest kubectl apply -f kubernetes/documents-persistentvolumeclaim.yaml
	docker run -v kube:/root/.kube -v aws:/root/.aws -v $(ROOT_DIR):/apps nassila94h/k8s_aws:latest kubectl apply -f kubernetes/db-data-persistentvolumeclaim.yaml
	docker run -v kube:/root/.kube -v aws:/root/.aws -v $(ROOT_DIR):/apps nassila94h/k8s_aws:latest kubectl apply -f kubernetes/config-persistentvolumeclaim.yaml
	docker run -v kube:/root/.kube -v aws:/root/.aws -v $(ROOT_DIR):/apps nassila94h/k8s_aws:latest kubectl get all

## —— Clean️ 🗑 ———————————————————————————————————————————————————————————————————
clean: ## Destroy cluster
	docker run -v kube:/root/.kube -v aws:/root/.aws -v $(ROOT_DIR):/apps nassila94h/k8s_aws:latest kubectl delete --all pods
	docker run -v kube:/root/.kube -v aws:/root/.aws -v $(ROOT_DIR):/apps nassila94h/k8s_aws:latest kubectl delete --all services
	docker run -v kube:/root/.kube -v aws:/root/.aws -v $(ROOT_DIR):/apps nassila94h/k8s_aws:latest kubectl delete --all deployments
	docker run -v kube:/root/.kube -v aws:/root/.aws -v $(ROOT_DIR):/apps nassila94h/k8s_aws:latest kubectl delete --all pvc
	docker run -v kube:/root/.kube -v aws:/root/.aws -v $(ROOT_DIR):/apps nassila94h/k8s_aws:latest eksctl delete cluster --name projet-group2-dolibarr --region eu-west-3

## —— Get services———————————————————————————————————————————————————————————————————
get_url:
	docker run -v kube:/root/.kube -v aws:/root/.aws -v $(ROOT_DIR):/apps nassila94h/k8s_aws:latest kubectl get svc

## —— Get all resources ———————————————————————————————————————————————————————————————————
get_all:
	docker run -v kube:/root/.kube -v aws:/root/.aws -v $(ROOT_DIR):/apps nassila94h/k8s_aws:latest kubectl get all

## —— Delete pod 🗑 ———————————————————————————————————————————————————————————————————
delete_pod:
	docker run -v kube:/root/.kube -v aws:/root/.aws -v $(ROOT_DIR):/apps nassila94h/k8s_aws:latest kubectl delete pod $(POD_ID)

## —— Others 🛠️️ ———————————————————————————————————————————————————————————————————
help: ## Liste des commandes
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'