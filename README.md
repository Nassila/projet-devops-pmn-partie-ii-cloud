# Projet-DevOps-PMN-Partie-II-Cloud

Ce depôt git est la procedure d'installation de l'ERP Dolibarr (lien)

Ce projet est réalisé grâce à un provisionning automatisé sur une infrastructure “Cloud” en haute disponibilité

Outils et technologies utilisés sont :
1) Docker
2) Kubernetes
3) Make
3) AWS - EKS

##Démarche

1) Création de l'image Dolibarr et la pousser dans le DockerHub

2) Mise en place du docker-compose (Dolibarr + mariadb)

3) Création d'une image k8S contenant tous les outils nécessaires pour pouvoir déployer un cluster kubernetes sur AWS-EKS

4) Génération des fichiers yaml (service, deployment, pvc) l'aide du kompose

5) Déploiement du cluster

6) Mise en place d'un makefile pour automatiser les différentes étapes d'execution

7) Mise en place du déploiement continu

##Procedure d'installation:

clone le projet
```
git clone https://gitlab.com/Nassila/projet-devops-pmn-partie-ii-cloud.git
cd projet-devops-pmn-partie-ii-cloud
```

### Pour lancer Dolibarr à partir du cluster kubernetes
```
make deploy
```
recuperer l'external-ip du load balancer en lançant :
```
make get_url
```
et la coller dans le navigateur pour se connecter sur Dolibarr

### installation 
suivre les étapes d'installation :

choisir la langue -> suivant -> demarrer 

entrer les données :

```
Nom de la base de données = database
Serveur de base de données = mariadb 
Identifiant = user 
Mot de passe = password
```

suivre les étapes d'après jusqu'à la création d'un user 

Bravo! vous avez terminé l'installation de Dolibarr

NB: un fichier .gitlab-ci.yml est présent pour que à chaque `git push`on déclanche le rebuild des images concernées et 
une fois que le build de l’image réussi, alors la nouvelle version de l’image est déployée sur le cluster.